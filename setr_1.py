#!/usr/bin/python3

# Bu faylda sətrlərlə bağlı əldə etdiyiniz biliklərin
# möhkəmləndirilməsi üçün tapşırıqlar vardır.
# Aşağıda natamam funksiyalar verilmişdir. Bunu tamamlamalısınız.
# Bu fayl icra olunarkən tamamladığınız funksiyalar testi keçməlidir.
# Uğurlar !!!

# Tapşırıq 1.
# Aşağıdakı funksiyanın arqumenti ədəd qəbul edir.
# Verilmiş ədəddən asılı olaraq funksiya aşağıdakı kimi sətrlər qaytarmalıdır
# * Ədəd 10-a qədər olduqda  - "Hədiyyə sayı: <ədəd>" Məsələn "Hədiyyə sayı: 5"
# * Ədəd 10 və daha çox olduqda  - "Hədiyyə sayı: çox"

def hediyye(say):
	s='Hədiyyə sayı: '
	if say<0:
		print("Hədiyyə 0-dan kiçik ola bilməz!")
	if 0<say<10:
		print(s, say)
	if say >= 10:
		print(s+' çox')
	return

# Tapşırıq 2.
# Aşağıdakı funksiyanın arqumenti sətr qəbul edir.
# Verilmiş funksiya arqumentdə qəbul etdiyi sözün ilk hərfini əldə edib
# ilk hərfdən başqa rast gələn bütün yerlərdə * işarəsi ilə əvəzləyib
# alınan sətri qaytarmalıdır
# Məsələn "lalələr" -- "la*ə*ər"

def soz_evezle(soz):
	soz=soz.upper()
	f=soz[0]
	s_part=soz[1:len(soz)]
	s=s_part.replace(f,'*')
	print(f+s.lower())
	return


# Tapşırıq 3.
# Aşağıdakı funksiyanın arqumenti sətr qəbul edir.
# Verilmiş funksiyanın arqumentdə qəbul etdiyi söz vergüllə ayrılmış 2 sözdür. Məsələn "bıçaq, ranqola".
# Funksiya sözlərin ilk hərflərini, həmçinin sözlərin yerlərini dəyişib
# defislə ayrılmış şəkildə  qaytarmalıdır
# Məsələn "bıçaq, ranqola" -- "banqola-rıçaq"

def kasha(soz):
	list=soz.split(',')
	if len(list)==2:
		print(list[1], '-', list[0])
	else:
		print('Bu setr formata uyğun deyil! String 2 sozden ibaret olmalidir.')
	return

# Tapşırıq 3.
# Aşağıdakı funksiyanın arqumenti sətr qəbul edir.
# Sətrin uzunluğundan asılı olaraq söz ya felə ya da sifətə çevrilməlidir
# * Simvol sayı 4 və ondan kiçik olduqda  sonuna "-maq" şəkilçisi əlavə edilsin. Məsələn "qaç" - "qaçmaq"
# * Simvol sayı 4-dən çox olduqda sonuna "-li" şəkilçisi əlavə edilsin. Məsələn "çiçək" - "çiçəkli"

def soz_duzelt(soz):
	s=''
	if len(soz)<=4:
		s=soz+'maq'
	if len(soz)>4:
		s=soz+'li'
	print(s)
	return

# Tapşırıq 4.
# Aşağıdakı funksiyanın arqumenti sətr qəbul edir.
# Verilmiş funksiya arqumentdə qəbul etdiyi sözün son simvolunun "*" olmadığını halda
# son "*" simvolundan sonrakı simvol ilə bütün "*" simvollarını əvəz edib
# alınan sətri qaytarmalıdır
# Məsələn "a*b*c*xd" - "axbxcxd"

def soz_tamamla(soz):
	if not soz[len(soz)-1]=='*':
		soz=soz.replace('*', 'x')
		print(soz)
	else:
		print('Setr sherte uygun deyil!')

	return
