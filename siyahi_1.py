#!/usr/bin/python3

from operator import itemgetter, attrgetter, methodcaller

# Bu faylda siyahılarla bağlı əldə etdiyiniz biliklərin
#möhkəmləndirilməsi üçün tapşırıqlar vardır.
#Aşağıda natamam funksiyalar verilmişdir. Bunu tamamlamalısınız.
#Uğurlar !!!

# Tapşırıq 1.
# Aşağıdakı funksiya arqumentdə uyğun olaraq yığım və sətr qəbul edir.
# verilmiş yığım elementləri sətrlərdən ibarətdir
# funksiya son iki simvolu ikinci arqumentlə uyğun gələn elementlərin sayını qaytarmalıdır
# uygun_say(('say','yay','buz'), 'ay') -> 2
from builtins import print


def uygun_say(yigim, uygun_setr):
	i=0
	y=0 #uygun elementi mueyyen eden deyiwen
	for i in range(len(yigim)):
		part2=yigim[i]
		part2=part2[len(part2)-2:]
		if part2==uygun_setr:
			y+=1
	print('Uygun elementlerin sayi: ', y)

	return

#uygun_say(('qqqqqqqsay','ywwwwwwwwwway','buz'), 'ay')

# Tapşırıq 2.
# Aşağıdakı funksiya arqumentdə 3 elementli yığım siyahısı qəbul edir.
# siyahıdakı hər element (<ad>, <yaş>, <kitab>) formatındadır-> ('Rasim',12,'Yerin mərkəzinə sayahət')
# funksiya siyahının elementlərini, hər elementdə qeyd olunmuş yaşa görə sortlaşdırmalı və
# buna uyğun kitablar siyahısını qaytarmalıdır.
# Köməkçi qeyd: sorted funksiyasının key arqumentindən istifadə etmək tövsiyə olunur.
# kitab_yash((('Rasim',12,'Quliver'), ('Adil', 6, 'Faradey'))) -> ['Faradey', 'Quliver']

def kitab_yash(yigim_siyahisi):
    yigim_siyahisi=sorted(yigim_siyahisi, key=itemgetter(1))
    i=0
    s=''
    list2=[]
    for i in range(len(yigim_siyahisi)):
        list1=yigim_siyahisi[i]
        list2.append(list1[2])
    print(list2)
    return

#kitab_yash((('Rasim',12,'Quliver'), ('Adil', 6, 'Faradey'), ('Elnur', 3, 'YAP partiyasinin tarixi'), ('Bayram', 5, 'Alibaba ve 40 quldur')))


# Tapşırıq 3.
# Aşağıdakı funksiya birinci arqumentdə ədələrdən ibarət siyahı qəbul edir.
# İkinci arqumentdə sətrlərdən ibarət siyahı qəbul edir.
# funksiya birinci siyahının ədədlərinə uyğun olaraq ikinci siyahını sıralayıb qaytarmalıdır
# siyahi_sirala((3,1,2),('Rasim', 'Qafar', 'Nadir')) -> ['Nadir', 'Rasim', 'Qafar']
# Qeyd: tapşırığı həll edərkən rast gələ biləcək əsas istifadəçi səhvlərini nəzərə almağınız tövsiyə olunur.

def siyahi_sirala(indeksler, siyahi):
    print(indeksler)
    print(siyahi)
    merge_list=zip(indeksler,siyahi)
    d=dict(merge_list)
    print(d)
    return

#siyahi_sirala((3,1,2),('Rasim', 'Qafar', 'Nadir'))


# Tapşırıq 4.
# Aşağıdakı funksiya birinci arqumentdə endirimə düşən malların siyahısını alır.
# İkinci arqumentdə (<mal>, <məbləğ>) fromatlı elementlərdən ibarət siyahı qəbul edir -> 
# [('Kitab', 43), ('Stol', 128)]
# Üçüncü arqumentdə tətbiq ediləcək endirim dərəcəsini qəbul edir.
# funksiya mal siyahısından endirimə düşən malları aşkar edib
# verilmiş dərəcədə endirib tətbiq etməli və cəmi endirim məbləğini qaytarmalıdır
# endirim_meblegi(('Stol','Qələm','Karandaş'), (('Kitab', 43), ('Stol', 128)), 0.12) -> 15.36

def endirim_meblegi(endirim_siyahi, mal_siyahi, derece):
    i=0
    ashkar_cem=0
    endirim_mebleg=0
    dict_mal=dict(mal_siyahi)
    for i in range(len(endirim_siyahi)):
        if endirim_siyahi[i] in dict_mal:
            ashkar_cem=ashkar_cem+dict_mal[endirim_siyahi[i]]


    print('Alınmış malların cəmi: '+str(ashkar_cem))
    endirim_mebleg=(ashkar_cem*derece)
    print('Endirim olunmuş məbləğ: '+str(endirim_mebleg))
    print('Ödənilməli məbləğ: ', ashkar_cem-endirim_mebleg)
    return

#endirim_meblegi(('Stol','Qələm','Karandaş'), (('Kitab', 43), ('Stol', 128), ('Qələm', 100)), 0.12)

# Tapşırıq 5.
# Aşağıdakı funksiya birinci arqumentdə 2 elementli yığım siyahısı qəbul edir.
# bu siyahının hər elementi (<mal>, <qiymət>) formatındadır-> ('Qarğıdalı', 5)
# ikinci arqument bütün siyahıya tətbiq ediləcək endirim məbləğidir
# funksiya ümumi məbləği qiymətə mütənasib bölməli və
# bölünmüş məbləğləri yüzdəbirlərə qədər yuvarlaqlaşdırıb siyahısını qaytarmalıdır
# endirim_paylama((('Kahı',12), ('Çuğundur', 45)),50) -> [10.53, 39.47]

def endirim_paylama(mal_siyahisi, endirim):
    i=0

    for i in range(len(mal_siyahisi)):
        list1=mal_siyahisi[i]
        mal_qiy=list1[1]
        mal_end=mal_qiy*endirim
        mal_result=mal_qiy-mal_end
        mal_round=round(mal_result,1)
        print('Malin qiymeti: '+str(mal_qiy)+' Endirimli qiymeti: '+str(mal_result)+' Yuvarlaqlashmish: '+ str(mal_round))

    return


#endirim_paylama((('Kahı',12), ('Çuğundur', 45)), 0.13)
